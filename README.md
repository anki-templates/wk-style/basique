# Basique

Parce que Anki peut aussi être très beau !

Simple modèle de carte avec un recto et un verso sans saisie de réponse aux couleurs de [wanikani](https://www.wanikani.com).

Vous pouvez ouvrir ce fichier de [tables de multiplication](/Tables_de_multiplication.apkg) avec Anki pour ajouter le type de note `WK-Style Sens Unique sans saisie` à Anki et créer vos propres cartes avec ce type de note.

## Captures d'écran

### Desktop

|Recto|Verso|
|:---:|:---:|
|![](/images/desktop-recto.png)|![](/images/desktop-verso.png)|

### AnkiMobile

|Recto|Verso|
|:---:|:---:|
|![](/images/ankimobile-recto.png)|![](/images/ankimobile-verso.png)|
